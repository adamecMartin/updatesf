/**
 * @file app.js Application entry point
 * @author Martin Adamec
 * @copyright 2021
 */

const express = require('express');
const bodyParser = require('body-parser');

const config = require('./config');
const { routes } = require('./routes');

const { log, cleanup, conn } = require('./cscommon/index');

cleanup.Cleanup();
 
const app = express();

app.use(express.json());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use('/', routes);

module.exports = app.listen(config.port, () => {
console.log(`Listening on localhost: ${config.port}`);
});
 
 