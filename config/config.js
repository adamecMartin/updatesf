const {
    APP_CONFIG_PORT,
    LOG_DIRECTORY,
    LOG_MAX_FILES,
    LOG_MAX_SIZE
} = require("../models");

// DEV
// const systemSF = {
//     user: 'admin@successsolD',
//     odataUrl: "https://api2.successfactors.eu/odata/v2/",
//     pass: '2Sjede'
// };

// DEMO
const systemSF = {
    user: 'APIUSER@DEMO2S',
    odataUrl: "https://api2preview.sapsf.eu/odata/v2/",
    pass: 'apiuser'
};


const logger = {
    logDirectory: `${__dirname}/../${LOG_DIRECTORY}`,
    level: 'verbose',
    errorFilename: 'error-%DATE%.log',
    combinedFilename: 'combined-%DATE%.log',
    doubleLogs: true,
    consoleLog: true,
    maxSize: LOG_MAX_SIZE,
    maxFiles: LOG_MAX_FILES,
    datePattern: 'YYYY-MM-DD',
    project: 'edanovkaUpsertEmptyFields',
    zippedArchive: true,
    handleExceptions: true
}


module.exports = {
    port: APP_CONFIG_PORT,
    isProduction: false,
    systemSF,
    user: systemSF.user,
    pass: systemSF.pass,
    odataUrl: systemSF.odataUrl,
    TMspecific: false,
    logger
};