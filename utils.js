const request = require("request");

/**
 * 
 * @param {object} options
 */    
function doRequest(options) {
    return new Promise(function (resolve, reject) {
        request(options, function (error, res, body) {
            if (!error && res.statusCode >= 200 && res.statusCode < 300) {
                resolve(res);
            } else {
                reject(res);
            }
        });
    });
};

module.exports = {
    doRequest,
};
