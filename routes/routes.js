const express = require('express');
const routesMethods = require('./routes.methods');

const router = express.Router();

router.use('/api', routesMethods);

module.exports = router;