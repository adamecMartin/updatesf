const express = require('express');
const common = require('../common/index');

const config = require('../config');
const utils = require('../utils');
const models = require('../models');
const helperFunct = require('../helperFunct');

const router = express.Router();

const { connectionModule, logger } = global;

/**
 * Endpoint for anonymizing data. 
 * The endpoint receives the group parameter
 */
router.post('/updateHireDate', async (req, res) => {

      const conn = connectionModule;
      let sEntita = "cust_edan_masterData";
      let sSelect = "cust_hireDate, externalCode, cust_userId";
      let sFilter = "cust_hireDate eq null";

      let aMasterData = await conn.promise1000(async () => await helperFunct.callSF.getMasterData(conn, sEntita, sSelect, sFilter));
      let aUpsert = [];
      let nCounter = 0;
      for (item of aMasterData) {
            nCounter++;
            if(nCounter % 100 === 0) {
                  console.log('počet zaznamu: ' + nCounter);
            }
            var dateHire = await helperFunct.callSF.getUserHireDate(item);
            if(dateHire) {
                  item.cust_hireDate = dateHire;
                  item.__metadata = {
                        uri: 'cust_edan_masterData'
                  };
                  delete item.cust_userId;
                  aUpsert.push(item);
            }
      }
      let isUpsert = await common.upsertData.upsertMasterData(conn, aUpsert);
      if (!isUpsert) {
            res.status(500).send("ERROR: error occurred while executing the upsert.");
      }

      res.send("OK");   
});

module.exports = router;