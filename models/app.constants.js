
const LOG_DIRECTORY = 'log';
const LOG_MAX_SIZE = '100m';
const LOG_MAX_FILES = '14d';
const LOG_ERROR_FOLDER = 'edanovkaAddEmptyField/logs/';
const LOG_COMBINED_FOLDER = 'edanovkaAddEmptyField/logs';

const APP_CONFIG_PORT = 8080;

module.exports = {
    LOG_DIRECTORY,
    LOG_MAX_SIZE,
    LOG_MAX_FILES,
    LOG_ERROR_FOLDER,
    LOG_COMBINED_FOLDER,
    APP_CONFIG_PORT
}