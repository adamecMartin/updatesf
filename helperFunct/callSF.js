const config = require('../config');
const utils = require('../utils');
const { connectionModule, logger } = global;

/**
 * 
 * @param {Object} conn - object from cscommon
 * @param {String} sEntita 
 * @param {String} sSelect 
 * @returns Array
 */
async function getMasterData(conn, sEntita, sSelect, sFilter) {
    
    const params = {
        entity: `${sEntita}`,
        select: `${sSelect}`,
        filter: `${sFilter}`
      };
      return conn.createPromise(conn.createUrl(params));
};

async function getUserHireDate(record) {

    let oAuthorization = config.systemSF;

    let sAuth = oAuthorization.user + ':' + oAuthorization.pass; 
    let oBuff = new Buffer(sAuth);
    let sBase64 = oBuff.toString('base64');
    let sQueryOptions = `User?$format=JSON&$select=userId,hireDate&$filter=userId eq '${record.cust_userId}' and status in 'active','inactive'`; 
   
    let oOptions = {
        'method': 'GET',
        'url': oAuthorization.odataUrl + sQueryOptions,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + sBase64
        }
    };
    try {
        let oResponse = await utils.doRequest(oOptions);
        let oResponseBody = JSON.parse(oResponse.body); 
        if (oResponseBody.d.results.length === 0) {
            return null;
        } else {
            return oResponseBody.d.results[0].hireDate;

        }        
    } catch(err) { 
        console.log('ERROR: getUserHireDate(): ' + err);
        logger.error(`getUserHireDate(): ${err}`);
        return false;
    }
};

module.exports = {
    getMasterData,
    getUserHireDate
};