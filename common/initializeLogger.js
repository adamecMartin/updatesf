'use strict';
const fs = require('fs');
const { log } = require('../cscommon'); // only --- { log }

module.exports = function ({ config }) {
//create directory if it does not exist

const { logDirectory } = config.logger;

fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

const logClass = new log({
    level: config.logger.level,
    errorFilename: `${config.logger.logDirectory}/${config.logger.errorFilename}`,
    combinedFilename: `${config.logger.logDirectory}/${config.logger.combinedFilename}`,
    doubleLogs: config.logger.doubleLogs,
    consoleLog: config.logger.consoleLog,
    project: config.logger.project,
    zippedArchive: config.logger.zippedArchive,
    maxFiles: config.logger.maxFiles,
    maxSize: config.logger.maxSize,
    datePattern: config.logger.datePattern,
    handleExceptions: config.logger.handleExceptions
});

const loggerObj = logClass.getLogger;

return loggerObj;
}