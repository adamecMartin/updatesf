const { log, conn, fileManipulation } = require('../cscommon/index');
const config = require('../config');
const initializeLogger = require('./initializeLogger');

const initializeApp = (async () => {
    
    global.logger = initializeLogger({
        config
    });
    global.connectionModule = new conn({
        user: config.user,
        pass: config.pass,
        odataUrl: config.odataUrl
    }); 
    global.fileManipulation = new fileManipulation();
})();

module.exports = initializeApp;
