const { connectionModule, logger } = global;

/**
 * 
 * @param {Object} conn - object from cscommon
 * @param {Array} aUpsertData - array of all sensitive date
 * @return {Boolean} - true if  upsert was successfully performed
 */
const upsertMasterData = async (conn, aUpsertData) => {

    logger.info(`Data ready for upsert: ${JSON.stringify(aUpsertData)}`);

    if (aUpsertData.length === 0) {
        console.log('INFO: no change entry found.'); 
        logger.info(`No data was passed for the upsert.`);
        return true;
    } else if (aUpsertData.length > 0) {
        try {
            let oBatchRequest = await conn.createBatchRequest({
                entity: `upsert`,
                operation: 'POST',
                key: '',
                data: aUpsertData
            });
            let nCounterErr = 0; 
            oBatchRequest[0].forEach(element => { 
                if (!element.body.includes('"status" : "OK", "editStatus" : "UPSERTED"')) {
                    nCounterErr++;
                    console.log(JSON.stringify(element.body));
                }; 
            });
            return nCounterErr === 0 ? true : false;
        } catch(err) {
            logger.error(`ERROR: upsertMasterData(). Failed upsert. Err: ${err}`);
            console.log('ERROR: upsertMasterData(): ' + err);
            return false;
        }
    } 
};

module.exports = {
    upsertMasterData,
}